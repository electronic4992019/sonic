# SONIC 

Test board for ultrasonic sensor HC-SR04 using ATMEGA4808 and a 128x64 OLED screen

## PCB

Home made PCB

![alt text](doc/sonic_zoom.jpg)
![alt text](doc/sonic.jpg)

## 3D view

![alt text](doc/sonic-pcb.jpg)