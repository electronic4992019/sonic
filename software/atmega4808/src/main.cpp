#include "main.h"
#include "bitmap.h"

void setup() {
    /* Initialise le port série */
    Serial.begin(9600);

    /* init  LED */
    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, LOW);

    /* Init ultrasonic sensor */
    pinMode(PIN_TRIGGER, OUTPUT);
    digitalWrite(PIN_TRIGGER, LOW); // La broche TRIGGER doit être à LOW au repos
    pinMode(PIN_ECHO, INPUT);

    /*init display */
    display.begin();

    taskManager.schedule(repeatMillis(1000), DoBlinkLed);
    taskManager.schedule(repeatSeconds(1), DoMesureSonic);
}

void loop() {
    taskManager.runLoop();
}

void DoBlinkLed() {
    digitalWrite(PIN_LED, CHANGE);
}

void DoMesureSonic() {
    /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
    digitalWrite(PIN_TRIGGER, HIGH);
    delayMicroseconds(10);
    digitalWrite(PIN_TRIGGER, LOW);

    /* 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe) */
    long measure = pulseIn(PIN_ECHO, HIGH, MEASURE_TIMEOUT);

    /* 3. Calcul la distance à partir du temps mesuré */
    float distance_mm = measure / 2.0 * SOUND_SPEED;

    /* Affiche les résultats en mm, cm et m */
    Serial.print(F("Distance: "));
    Serial.print(distance_mm);
    Serial.print(F("mm ("));
    Serial.print(distance_mm / 10.0, 2);
    Serial.print(F("cm, "));
    Serial.print(distance_mm / 1000.0, 2);
    Serial.println(F("m)"));

    display.clear();
    display.setCursor(35, 0);
    display.print("Distance [cm]");
    display.draw_line(2,10,126,10);
    dtostrf(distance_mm / 10.0, 4, 2, cm_text);
    display.draw_string(40,20,cm_text,OLED::DOUBLE_SIZE);
    display.draw_bitmap(3,16, 24,24, bitmap);
    display.display();
}

