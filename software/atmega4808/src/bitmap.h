#include "Arduino.h"

// convert bitmap using : https://javl.github.io/image2cpp/  (1 byte vertical)
// remove 0x in front of each number
// convert hex to decimal with : https://www.convzone.com/hex-to-decimal/

static const uint8_t bitmap[] =
        {
                0, 0, 0, 240, 248, 152, 152, 120, 120, 24, 24, 248, 248, 24, 24, 120,
                120, 24, 24, 248, 240, 0, 0, 0, 0, 0, 0, 255, 255, 153, 153, 24,
                0, 0, 248, 252, 12, 12, 12, 12, 12, 12, 12, 15, 7, 0, 0, 0,
                0, 0, 0, 15, 31, 25, 25, 24, 24, 24, 31, 15, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0
        };