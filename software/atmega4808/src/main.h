

#ifndef ATMEGA4808_MAIN_H
#define ATMEGA4808_MAIN_H

#include <Arduino.h>
#include "TaskManagerIO.h"

#include <SPI.h>
#include <Wire.h>
#include "oled.h"

#define PIN_OLED_SDA PIN_PA2
#define PIN_OLED_SCL PIN_PA3
#define PIN_LED PIN_PC2
#define PIN_TRIGGER PIN_PD1
#define PIN_ECHO PIN_PD3
#define PIN_BTN_MESURE PIN_PA7
#define PIN_BTN_MODE PIN_PB0

/* Constantes pour le timeout */
const unsigned long MEASURE_TIMEOUT = 25000UL; // 25ms = ~8m à 340m/s
/* Vitesse du son dans l'air en mm/us */
const float SOUND_SPEED = 340.0 / 1000;
char cm_text[7]; // max size is xxx.xx (6 + 1)


/* general variables */
OLED display(PIN_OLED_SDA, PIN_OLED_SCL, NO_RESET_PIN, OLED::W_128, OLED::H_64);



/* function prototypes */
void setup();
void loop();
void DoBlinkLed();
void DoMesureSonic();
void DoModeBtn();
void DoMesureBtn();

#endif //ATMEGA4808_MAIN_H
