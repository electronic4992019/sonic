#include <Arduino.h>

#define PIN_LED PIN_PC2

void setup() {
    Serial.begin(9600);
    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, LOW);
}

void loop() {
    digitalWrite(PIN_LED, CHANGE);
    delay(1000);
    Serial.println("hello");
}
