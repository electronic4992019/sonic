#include <Arduino.h>
#include <TaskManagerIO.h>

#include <SPI.h>
#include <Wire.h>
#include "oled.h"


/* PINS USAGE
 * PIN_PB1 : SDA
 * PIN_PB0 : SCL
 * PIN_PB2 : TX
 * PIN_PB3 : RX
 * PIN_PA0 : UPDI
 * PIN_PA1 : TRIGGER
 * PIN_PA2 : ECHO
 * PIN_PA7 : LED */

#define PIN_LED PIN_PA7
#define PIN_TRIGGER PIN_PA1
#define PIN_ECHO PIN_PA2
#define PIN_OLED_SDA PIN_PB1
#define PIN_OLED_SCL PIN_PB0

OLED display(PIN_OLED_SDA, PIN_OLED_SCL, NO_RESET_PIN, OLED::W_128, OLED::H_64);
// guide for OLED display : https://randomnerdtutorials.com/guide-for-oled-display-with-arduino/

/* Constantes pour le timeout */
const unsigned long MEASURE_TIMEOUT = 25000UL; // 25ms = ~8m à 340m/s
/* Vitesse du son dans l'air en mm/us */
const float SOUND_SPEED = 340.0 / 1000;



void DoBlinkLed() {
    digitalWrite(PIN_LED, CHANGE);
    Serial.println("blink");
}

void DoMesureSonic() {
/* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
    digitalWrite(PIN_TRIGGER, HIGH);
    delayMicroseconds(10);
    digitalWrite(PIN_TRIGGER, LOW);

    /* 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (si il existe) */
    long measure = pulseIn(PIN_ECHO, HIGH, MEASURE_TIMEOUT);

    /* 3. Calcul la distance à partir du temps mesuré */
    float distance_mm = measure / 2.0 * SOUND_SPEED;

    /* Affiche les résultats en mm, cm et m */
    Serial.print(F("Distance: "));
    Serial.print(distance_mm);
    Serial.print(F("mm ("));
    Serial.print(distance_mm / 10.0, 2);
    Serial.print(F("cm, "));
    Serial.print(distance_mm / 1000.0, 2);
    Serial.println(F("m)"));

    display.clear();
    display.print(distance_mm / 10.0, 2);
    display.print(F("cm"));
    display.setCursor(0,10);
    display.print(distance_mm / 1000.0, 2);
    display.println(F("m"));
    display.display();
}

void setup() {
    /* Initialise le port série */
    Serial.begin(9600);

    display.begin();

    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, LOW);


    /* Initialise les broches */
    pinMode(PIN_TRIGGER, OUTPUT);
    digitalWrite(PIN_TRIGGER, LOW); // La broche TRIGGER doit être à LOW au repos
    pinMode(PIN_ECHO, INPUT);

    taskManager.schedule(repeatMillis(1000), DoBlinkLed);
    taskManager.schedule(repeatSeconds(1), DoMesureSonic);
}

void loop() {
    taskManager.runLoop();
}